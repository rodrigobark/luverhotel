﻿<%@ Page Title="Cadastre-se | Luver Hotel" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="cadastro.aspx.cs" Inherits="cadastro" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/function.js" type="text/javascript"></script>
    <script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="js/jquery.maskedinput-1.2.2.js" type="text/javascript"></script>
    
     <script type="text/javascript">
         jQuery(function ($) {
             $("#ContentPlaceHolder1_txtNascimento").mask("99/99/9999");
             $("#ContentPlaceHolder1_txtCep").mask("99999-999");
         });
 </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="conteudointernas">
        <div class="alinhaconteudointernas">
        <div class="titleinternas"><img src="imagens/title_trabalhe.png" /></div>
        <div class="internas">
        <div class="trabalhe">
            <div class="cadastro">
                <img src="imagens/reservasCadastro.jpg" />
            </div>

            <div class="trabalheFormulario">
                <table cellpadding="1" cellspacing="4">
                    <tr>
                        <td><img src="imagens/trabalheNome_.jpg" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtNome" runat="server" CssClass="cadastroNome"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rqvNome" runat="server" 
                            ControlToValidate="txtNome" Display="None" 
                            ErrorMessage="Por favor, preencha seu nome." SetFocusOnError="True" 
                            ValidationGroup="grpCadastro"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="imagens/trabalheEmail_.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtEmail" runat="server" CssClass="cadastroEmail"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rqvEmail" runat="server" 
                            ControlToValidate="txtEmail" Display="None" 
                            ErrorMessage="Por favor, preencha seu email." SetFocusOnError="True" 
                            ValidationGroup="grpCadastro"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="rgeEmail" runat="server" 
                            ControlToValidate="txtEmail" Display="None" 
                            ErrorMessage="Por favor, preencha corretamente seu e-mail." 
                            SetFocusOnError="True" 
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                            ValidationGroup="grpCadastro"></asp:RegularExpressionValidator>
                            </td>
                    </tr>
                    <tr>
                        <td><img src="imagens/cDatadeNascimento.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtNascimento" runat="server" CssClass="cadastroNascimento"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rqvDataDeNascimento" runat="server" 
                            ControlToValidate="txtNascimento" Display="None" 
                            ErrorMessage="Por favor, preencha sua data de nascimento." 
                            SetFocusOnError="True" ValidationGroup="grpCadastro"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="rgeDataDeNascimento" runat="server" 
                            ControlToValidate="txtNascimento" Display="None" 
                            ErrorMessage="Por favor, preencha corretamente sua data de nascimento." 
                            SetFocusOnError="True" 
                            ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$" 
                            ValidationGroup="grpCadastro"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="imagens/cRg.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtRg" runat="server" CssClass="cadastroRg"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td> <img src="imagens/cCpf.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtCpf" runat="server" CssClass="cadastroCpf"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rqvCPFCNPJ" runat="server" 
                            ControlToValidate="txtCpf" Display="None" 
                            ErrorMessage="Por favor, preencha seu CPF ou CNPJ." SetFocusOnError="True" 
                            ValidationGroup="grpCadastro"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cstvCPFCNPJ" runat="server" 
                            ClientValidationFunction="valida_CPFCNPJ" ControlToValidate="txtCpf" 
                            Display="None" ErrorMessage="Por favor, preencha corretamente seu CPF ou CNPJ." 
                            SetFocusOnError="True" ValidationGroup="grpCadastro"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="imagens/cCep.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtCep" runat="server" CssClass="cadastroCep"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><img src="imagens/cEndereco.jpg" /></td>
                        <td><img src="imagens/cN.jpg" /></td>
                        <td> <img src="imagens/cComplemento.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtEndereco" runat="server" CssClass="cadastroEndereco"></asp:TextBox></td>
                        <td><asp:TextBox ID="txtNumero" runat="server" CssClass="cadastroN"></asp:TextBox></td>
                        <td><asp:TextBox ID="txtComplemento" runat="server" CssClass="cadastroComplemento"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><img src="imagens/cBairro.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtBairro" runat="server" CssClass="cadastroBairro"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><img src="imagens/cCidade.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtCidade" runat="server" CssClass="cadastroCidade"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><img src="imagens/cConheceCharms.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtConhece" runat="server" CssClass="cadastroConheceCharms"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><img src="imagens/cSenha.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtSenha" runat="server" CssClass="cadastroSenha" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rqvSenha" runat="server" 
                            ControlToValidate="txtSenha" Display="None" 
                            ErrorMessage="Por favor, preencha sua senha." SetFocusOnError="True" 
                            ValidationGroup="grpCadastro"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="imagens/cCSenha.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtConfirmarSenha" runat="server" CssClass="cadastroCSenha" TextMode="Password"></asp:TextBox>
                        <asp:CompareValidator ID="cpvSenha" runat="server" 
                            ControlToCompare="txtConfirmarSenha" ControlToValidate="txtSenha" Display="None" 
                            ErrorMessage="Por favor, confirme corretamente sua senha." 
                            SetFocusOnError="True" ValidationGroup="grpCadastro"></asp:CompareValidator>
                            </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnEnviar" runat="server" onclick="btnEnviar_Click" ValidationGroup="grpCadastro" ImageUrl="imagens/submit.jpg" />
                        </td>
                    </tr>
                            
                </table>
                <asp:ValidationSummary ID="vlds" runat="server" DisplayMode="List" 
            ShowMessageBox="True" ShowSummary="False" ValidationGroup="grpCadastro" />
            </div>
        </div>
        </div>
        </div>
    </div>
</asp:Content>

