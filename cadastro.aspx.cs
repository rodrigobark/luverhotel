﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using luverModel;

public partial class cadastro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnEnviar_Click(object sender, ImageClickEventArgs e)
    {
        var data = new luverEntities();
        var checacadastro = (from c in data.tbcadastro where c.email == txtEmail.Text select c).Count();
        if(checacadastro > 0)
        {
            AlertShow("Seu e-mail já está cadastrado em nosso sistema");
            return;
        }

        var cadastrar = new tbcadastro();
        cadastrar.nome = txtNome.Text;
        cadastrar.email = txtEmail.Text;
        cadastrar.dtNascimento = txtNascimento.Text;
        cadastrar.rg = txtRg.Text;
        cadastrar.cpf = txtCpf.Text;
        cadastrar.cep = txtCep.Text;
        cadastrar.endereco = txtEndereco.Text;
        cadastrar.numero = txtNumero.Text;
        cadastrar.complemento = txtComplemento.Text;
        cadastrar.bairro = txtBairro.Text;
        cadastrar.cidade = txtCidade.Text;
        cadastrar.conhece = txtConhece.Text;
        cadastrar.senha = txtSenha.Text;
        data.AddTotbcadastro(cadastrar);
        data.SaveChanges();
        AlertShow("Cadastro efetuado com sucesso");

        txtNome.Text = "";
        txtEmail.Text = "";
        txtNascimento.Text = "";
        txtRg.Text = "";
        txtCpf.Text = "";
        txtCep.Text = "";
        txtEndereco.Text = "";
        txtNumero.Text = "";
        txtComplemento.Text = "";
        txtBairro.Text = "";
        txtCidade.Text = "";
        txtConhece.Text = "";
        txtSenha.Text = "";
    }

    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "');", true);
    }
}