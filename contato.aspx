﻿<%@ Page Title="Contato | Luver Hotel" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="contato.aspx.cs" Inherits="contato" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="conteudointernas">
        <div class="alinhaconteudointernas">
        <div class="titleinternas"><img src="imagens/title_contato.png" /></div>
            <div class="internas">
            <div class="trabalhe">
            <div class="trabalheTextoDesc">
                <div class="contatoEnd">Rua Frei Caneca, nº. 963 - Cerqueira César - São Paulo - SP</div>
                <div class="contatoTel">(11) 3262-3375</div>
                <div class="contatoMapa">
                
                <iframe width="279" height="123" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                 src="http://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+Frei+Caneca,+n%C2%BA.+963+-+Cerqueira+C%C3%A9sar+-+S%C3%A3o+Paulo+-+SP&amp;aq=&amp;sll=-14.239424,-53.186502&amp;sspn=46.543562,86.572266&amp;ie=UTF8&amp;hq=Rua+Frei+Caneca,+n%C2%BA.+963+-+Cerqueira+C%C3%A9sar+-+S%C3%A3o+Paulo+-+SP&amp;hnear=&amp;radius=15000&amp;t=m&amp;cid=17070050512905465875&amp;ll=-23.555569,-46.656017&amp;spn=0.009677,0.023947&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
                 <br /><small><a href="http://maps.google.com.br/maps?f=q&amp;source=embed&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+Frei+Caneca,+n%C2%BA.+963+-+Cerqueira+C%C3%A9sar+-+S%C3%A3o+Paulo+-+SP&amp;aq=&amp;sll=-14.239424,-53.186502&amp;sspn=46.543562,86.572266&amp;ie=UTF8&amp;hq=Rua+Frei+Caneca,+n%C2%BA.+963+-+Cerqueira+C%C3%A9sar+-+S%C3%A3o+Paulo+-+SP&amp;hnear=&amp;radius=15000&amp;t=m&amp;cid=17070050512905465875&amp;ll=-23.555569,-46.656017&amp;spn=0.009677,0.023947&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left"><div class="contatoFundoMapa"></a><a href="javascript:void(0)" onclick="$('#mapa').fadeIn();"><img src="imagens/ampliarMapa.jpg" /></a></div></small>
                
                </div>
                
            </div>

            <div class="trabalheFormulario">
                <table cellpadding="1" cellspacing="5">
                    <tr>
                        <td><img src="imagens/trabalheNome_.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtNome" runat="server" CssClass="trabalheNome"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rqvNome" runat="server" 
                            ControlToValidate="txtNome" Display="None" 
                            ErrorMessage="Por favor, preencha seu nome." SetFocusOnError="True" 
                            ValidationGroup="grpCadastro"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="imagens/trabalheEmail_.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtEmail" runat="server" CssClass="trabalheEmail"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rqvEmail" runat="server" 
                            ControlToValidate="txtEmail" Display="None" 
                            ErrorMessage="Por favor, preencha seu email." SetFocusOnError="True" 
                            ValidationGroup="grpCadastro"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="rgeEmail" runat="server" 
                            ControlToValidate="txtEmail" Display="None" 
                            ErrorMessage="Por favor, preencha corretamente seu e-mail." 
                            SetFocusOnError="True" 
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                            ValidationGroup="grpCadastro"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td> <img src="imagens/trabalheTelefone_.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtTelefone" runat="server" CssClass="trabalheTelefone"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><img src="imagens/trabalheMsg_.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtMensagem" runat="server" CssClass="trabalheMensagem" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td> 
                            <asp:ImageButton ID="btnEnviar" runat="server" ValidationGroup="grpCadastro" ImageUrl="imagens/submit.jpg" 
                                onclick="btnEnviar_Click" />
                        </td>
                    </tr>
                </table>
                      <asp:ValidationSummary ID="vlds" runat="server" DisplayMode="List" 
            ShowMessageBox="True" ShowSummary="False" ValidationGroup="grpCadastro" />      

            </div>
        </div>
            </div>
        </div>
    </div>
</asp:Content>

