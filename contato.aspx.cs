﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class contato : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnEnviar_Click(object sender, ImageClickEventArgs e)
    {
        StringBuilder conteudo = new StringBuilder();


        conteudo.Append("O Srº(ª) " + "<b>" + txtNome.Text + "</b>" + " entrou em contato pelo site no dia " + DateTime.Now.ToShortDateString() + " às " + DateTime.Now.ToShortTimeString() + "hrs" + "<br /><br />");
        conteudo.Append("<b>" + "Nome do contato: " + "</b>" + txtNome.Text + "<br />");
        conteudo.Append("<b>" + "Email: " + "</b>" + txtEmail.Text + "<br />");
        conteudo.Append("<b>" + "Telefone: " + "</b>" + txtTelefone.Text + "<br />");
        conteudo.Append("<b>" + "Mensagem: " + "</b><br />" + txtMensagem.Text);
        conteudo.Append("</font>");

        MailMessage msg = new MailMessage("luver@luverhotel.com.br", "luver@luverhotel.com.br");
        msg.Subject = "Contato: www.luverhotel.com.br";
        msg.SubjectEncoding = Encoding.Default;
        msg.Body = conteudo.ToString();
        msg.BodyEncoding = Encoding.Default;
        msg.IsBodyHtml = true;

        try
        {
            msg.ReplyTo = new MailAddress(txtEmail.Text);
        }
        catch (Exception)
        {

        }

        var smtpClient = new SmtpClient("localhost");
        //smtpClient.EnableSsl = true;
        //smtpClient.Credentials = new System.Net.NetworkCredential("login", "senha");
        try
        {
            smtpClient.Send(msg);
            AlertShow("Mensagem enviada com sucesso. Agradecemos seu contato.");
            txtNome.Text = "";
            txtTelefone.Text = "";
            txtEmail.Text = "";
            txtMensagem.Text = "";
        }
        catch (Exception)
        {
            AlertShow("Falha ao enviar mensagem. Por favor, tente novamente.");
        }

    }

    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "');", true);
    }
}