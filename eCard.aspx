﻿<%@ Page Title="Envie para um amigo | Luver Hotel" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="eCard.aspx.cs" Inherits="eCard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="conteudointernas">
        
        <div class="titleinternas"><img src="imagens/title_recomende.png" /></div>
            <div class="internas">

            <div class="trabalhe">

            <div class="trabalheTextoDesc">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec nibh quis
                nulla tempus varius at vitae erat. Proin commodo, dolor eget laoreet mollis, 
                est sem sodales metus, eu vehicula quam ligula et tellus. Praesent lacus odio, 
                scelerisque molestie semper a, feugiat ac tortor.
            </div>

            
            <div class="trabalheFormulario">
            
                <table cellpadding="1" cellspacing="5">
                    <tr>
                        <td><img src="imagens/seu_nome.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtNome" runat="server" CssClass="trabalheNome"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rqvNome" runat="server" 
                            ControlToValidate="txtNome" Display="None" 
                            ErrorMessage="Por favor, preencha seu nome." SetFocusOnError="True" 
                            ValidationGroup="grpCadastro"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="imagens/nome_amigo.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtNomeAmigo" runat="server" CssClass="trabalheNome"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="txtNomeAmigo" Display="None" 
                            ErrorMessage="Por favor, preencha o nome de seu amigo." SetFocusOnError="True" 
                            ValidationGroup="grpCadastro"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td> <img src="imagens/email_amigo.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtEmailAmigo" runat="server" CssClass="trabalheNome"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rqvEmail" runat="server" 
                            ControlToValidate="txtEmailAmigo" Display="None" 
                            ErrorMessage="Por favor, preencha seu email." SetFocusOnError="True" 
                            ValidationGroup="grpCadastro"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="rgeEmail" runat="server" 
                            ControlToValidate="txtEmailAmigo" Display="None" 
                            ErrorMessage="Por favor, preencha corretamente seu e-mail." 
                            SetFocusOnError="True" 
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                            ValidationGroup="grpCadastro"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td><img src="imagens/trabalheMsg_.jpg" /></td>
                    </tr>
                    <tr>
                        <td><asp:TextBox ID="txtMensagem" runat="server" CssClass="trabalheMensagem" TextMode="MultiLine"></asp:TextBox></td>
                    </tr>                 
                    <tr>
                        <td>
                            <asp:ImageButton ID="btnEnviar" runat="server" ValidationGroup="grpCadastro" ImageUrl="imagens/submit.jpg" 
                                onclick="btnEnviar_Click" />
                                <asp:ValidationSummary ID="vlds" runat="server" DisplayMode="List" 
            ShowMessageBox="True" ShowSummary="False" ValidationGroup="grpCadastro" />
                         </td>
                    </tr>
                </table>
                            

            
                 </div>

            
            </div>
        </div>
    </div>
</asp:Content>

