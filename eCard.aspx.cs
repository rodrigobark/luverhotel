﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eCard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnEnviar_Click(object sender, ImageClickEventArgs e)
    {
        StringBuilder conteudo = new StringBuilder();


        conteudo.Append("<html><body><link href='http://www.luverhotel.com.br/includes/principal.css' rel='stylesheet' type='text/css' /><table width=450 border=0 align=center cellpadding=0 cellspacing=0><tr><td height=91><img src='http://www.luverhotel.com.br/images/topoIndique.jpg' width=450 height=202></td></tr><tr valign=top><td align=left background='http://www.luverhotel.com.br/images/bgPopUp.jpg' style=padding:20px;><table width=100% border=0 cellspacing=0 cellpadding=0><tr><td height=30 valign=top class=tahoma11-925E99>Seu amigo(a) " + txtNome.Text + "  lhe enviou este site Ecard Virtual </td></tr><tr><td align=center class=tahoma11-000000><table border=0 cellpadding=0 cellspacing=0 bgcolor=#E4D7E6><tr><td style=padding:5px;><table border=0 cellpadding=2 cellspacing=2 bgcolor=#955EA1><tr><td bgcolor=#E4D7E6 style=padding:5px;><a href='http://www.luverhotel.com.br/' target=_blank></a></td></tr></table></td></tr></table></td></tr><tr><td class=tahoma11-925E99>&nbsp;</td></tr><tr><td class=tahoma11-925E99>" + txtMensagem.Text + "</td></tr></table></td></tr><tr><td height=48><img src='http://www.luverhotel.com.br/images/rodapePopUp.jpg' width=450 height=48></td></tr></table></body></html>");

        MailMessage msg = new MailMessage("luver@luverhotel.com.br", txtEmailAmigo.Text);
        msg.Subject = "Luver Hotel";
        msg.SubjectEncoding = Encoding.Default;
        msg.Body = conteudo.ToString();
        msg.BodyEncoding = Encoding.Default;
        msg.IsBodyHtml = true;

        try
        {
            msg.ReplyTo = new MailAddress(txtEmailAmigo.Text);
        }
        catch (Exception)
        {

        }

        var smtpClient = new SmtpClient("localhost");
        //smtpClient.EnableSsl = true;
        //smtpClient.Credentials = new System.Net.NetworkCredential("login", "senha");
        try
        {
            smtpClient.Send(msg);
            AlertShow("Mensagem enviada com sucesso. Agradecemos seu contato.");
            txtNome.Text = "";
            txtMensagem.Text = "";
        }
        catch (Exception)
        {
            AlertShow("Falha ao enviar mensagem. Por favor, tente novamente.");
        }

    }

    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "');", true);
    }
}