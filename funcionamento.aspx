﻿<%@ Page Title="Funcionamento | Luver Hotel" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="funcionamento.aspx.cs" Inherits="funcionamento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="conteudointernas">
        <div class="alinhaconteudointernas">
        <div class="titleinternas"><img src="imagens/title_funcionamento.png" /></div>
            <div class="internas">
            <div class="funcionamentoReservas">
                <img src="imagens/reservas.jpg" />
            </div>
            <div class="funcionamentoInformacoes">
                <img src="imagens/24hrsdia.jpg" />

                <div class="funcionamentoServicoBar">
                    <img src="imagens/servicoBar.jpg" /><br />
                    24 horas
                </div>

                <div class="funcionamentoServicoLanchonete">
                    <img src="imagens/servicoLanchonete.jpg" /><br />
                    de domingo a quinta até as 00:00<br />
                    E sexta, sábado até as 02:00. (sujeito a confirmação)
                </div>
                
                <div class="funcionamentoPernoite">
                    <img src="imagens/pernoite.jpg" /><br />
                    Com permanência de 12 horas.<br />
                    Com preços promocionais de domingo a quinta.<br />
                    Com início as 20 horas.
                </div>

                <div class="funcionamentoDiaria">
                    <img src="imagens/diarias.jpg" /><br />
                    De domingo a quinta-feira
                </div>

                <div class="funcionamentoCartoes">
                    <img src="imagens/cartoes.jpg" /><br />
                    Visa, Mastercard, Redshop, Visa Electron e Dinners
                </div>
            </div>
            </div>
        </div>
    </div>
</asp:Content>

