﻿<%@ Page Title="Parceiros | Luver Hotel" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="parceiros.aspx.cs" Inherits="parceiros" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="conteudointernas">
        <div class="alinhaconteudointernas">
        <div class="titleinternas"><img src="imagens/title_parceiros.png" /></div>
            <div class="internas">
            
            <div class="parceirosConfira">
                <div class="parceirosConfiraFoto">
                    <img src="imagens/parceiros01.jpg" />
                </div> 
                <div class="parceirosConfiraDesc">
                <div class="parceirosConfiraDescTitle">Gonna - Future Jeans </div>
                 Desde 1997 hoje em três endereços na Baixada Santista, Shoppings Praiamar e Miramar
                  em Santos e em São Vicente na Praça da Bandeira.<br />
                    <a href="http://www.gonna.com.br">www.gonna.com.br</a>
                </div>
                <div class="parceirosBarra"><img src="imagens/barraFestival.jpg" /></div>
            </div>

            <div class="parceirosConfira">
                <div class="parceirosConfiraFoto">
                    <img src="imagens/parceiros02.jpg" />
                </div> 
                <div class="parceirosConfiraDesc">
                <div class="parceirosConfiraDescTitle">Charm's Motel</div>
                Motel  localizado em São Vicente, litoral de São Paulo. Possui as suítes mais confortáveis e modernas da Baixada Santista.<br />
                <a href="http://www.charmsmotel.com.br">www.charmsmotel.com.br</a>
                </div>
                <div class="parceirosBarra"><img src="imagens/barraFestival.jpg" /></div>
            </div>
            
            <div class="parceirosConfira">
                <div class="parceirosConfiraFoto">
                    <img src="imagens/parceiros03.jpg" />
                </div> 
              <div class="parceirosConfiraDesc">
                <div class="parceirosConfiraDescTitle">The Place Hotel</div>
                Hotel localizado na Consolação em São Paulo. Ótimos preços e promoções todos os dias.<br />
                <a href="http://www.theplacehotel.com.br">www.theplacehotel.com.br</a>
                </div>
                <div class="parceirosBarra"><img src="imagens/barraFestival.jpg" /></div>
            </div>
            
            </div>
        </div>
    </div>
</asp:Content>


