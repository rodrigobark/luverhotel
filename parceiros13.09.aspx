﻿<%@ Page Title="Parceiros | Luver Hotel" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="parceiros.aspx.cs" Inherits="parceiros" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="conteudointernas">
        <div class="alinhaconteudointernas">
        <div class="titleinternas"><img src="imagens/title_parceiros.png" /></div>
            <div class="internas">
            
            <div class="parceirosConfira">
                <div class="parceirosConfiraFoto">
                    <img src="imagens/parceiros01.jpg" />
                </div> 
                <div class="parceirosConfiraDesc">
                <div class="parceirosConfiraDescTitle">Gonna - Future Jeans </div>
                 Desde 1997 hoje em três endereços na Baixada Santista, Shoppings Praiamar e Miramar
                  em Santos e em São Vicente na Praça da Bandeira.<br />
                    <a href="http://www.gonna.com.br">www.gonna.com.br</a>
                </div>
                <div class="parceirosBarra"><img src="imagens/barraFestival.jpg" /></div>
            </div>

            <div class="parceirosConfira">
                <div class="parceirosConfiraFoto">
                    <img src="imagens/parceiros02.jpg" />
                </div> 
                <div class="parceirosConfiraDesc">
                <div class="parceirosConfiraDescTitle">Cadillac - Vintage Bar</div>
                 Esqueça tudo o que você já viu na noite, você esta prestes a embarcar em uma viagem atráves de todas as décadas!<br />                 Situada no Centro Histórico de Santos, trata-se de um novo conceito de casa noturna  
                    <a href="http://www.cadillacbar.com.br">www.cadillacbar.com.br</a>
                </div>
                <div class="parceirosBarra"><img src="imagens/barraFestival.jpg" /></div>
            </div>
            
            </div>
        </div>
    </div>
</asp:Content>


