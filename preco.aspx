﻿<%@ Page Title="Preços | Luver Hotel" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="preco.aspx.cs" Inherits="preco" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="conteudointernas">
        <div class="alinhaconteudointernas">
        <div class="titleinternas"><img src="imagens/title_precos.png" /></div>
            <div class="internas">
                <div class="precosDomingoQuinta">

                <div class="precosColunaPrimeira">
                    <div class="precosColunaPrimeiraDetLin">Suites</div>
                    <div class="precosColunaPrimeiraDet">PARIS</div>
                    <div class="precosColunaPrimeiraDet">PIGALLE</div>
                    <div class="precosColunaPrimeiraDet">LAMOUR</div>
                    <div class="precosColunaPrimeiraDet">LOUVRE</div>
                    <div class="precosColunaPrimeiraDet">LEMOLIN</div>
                    <div class="precosColunaPrimeiraDet">ROUGE</div>
                </div>

                <div class="precosColunaSegunda">
                    <div class="precosColunaSegundaDetLin">Período</div>
                    <div class="precosColunaSegundaDet">70,00</div>
                    <div class="precosColunaSegundaDet">75,00</div>
                    <div class="precosColunaSegundaDet">90,00</div>
                    <div class="precosColunaSegundaDet">95,00</div>
                    <div class="precosColunaSegundaDet">100,00</div>
                    <div class="precosColunaSegundaDet">183,00</div>
                </div>
                    
                <div class="precosColunaTerceira">
                    <div class="precosColunaTerceiraDetLin">Pernoite</div>
                    <div class="precosColunaTerceiraDet">100,00</div>
                    <div class="precosColunaTerceiraDet">105,00</div>
                    <div class="precosColunaTerceiraDet">120,00</div>
                    <div class="precosColunaTerceiraDet">125,00</div>
                    <div class="precosColunaTerceiraDet">145,00</div>
                    <div class="precosColunaTerceiraDet">250,00</div>
                </div>


                <div class="precosColunaQuarta">
                    <div class="precosColunaQuartaDetLin">Diária</div>
                    <div class="precosColunaQuartaDet">145,00</div>
                    <div class="precosColunaQuartaDet">155,00</div>
                    <div class="precosColunaQuartaDet">165,00</div>
                    <div class="precosColunaQuartaDet">180,00</div>
                    <div class="precosColunaQuartaDet">200,00</div>
                    <div class="precosColunaQuartaDet">260,00</div>
                </div>

                <div class="precosColunaQuarta">
                    <div class="precosColunaQuartaDetLin">Horas Adicionais</div>
                    <div class="precosColunaQuartaDet">26,00</div>
                    <div class="precosColunaQuartaDet">28,00</div>
                    <div class="precosColunaQuartaDet">30,00</div>
                    <div class="precosColunaQuartaDet">32,00</div>
                    <div class="precosColunaQuartaDet">39,00</div>
                    <div class="precosColunaQuartaDet">-</div>
                </div>

                </div>

                <div class="precosSextaSabado">

                <div class="precosColunaPrimeira">
                    <div class="precosColunaPrimeiraDetLin">Suites</div>
                    <div class="precosColunaPrimeiraDet">PARIS</div>
                    <div class="precosColunaPrimeiraDet">PIGALLE</div>
                    <div class="precosColunaPrimeiraDet">LAMOUR</div>
                    <div class="precosColunaPrimeiraDet">LOUVRE</div>
                    <div class="precosColunaPrimeiraDet">LEMOLIN</div>
                    <div class="precosColunaPrimeiraDet">ROUGE</div>
                </div>
                    

                <div class="precosColunaSegunda">
                    <div class="precosColunaSegundaDetLin">Período</div>
                    <div class="precosColunaSegundaDet">70,00</div>
                    <div class="precosColunaSegundaDet">75,00</div>
                    <div class="precosColunaSegundaDet">85,00</div>
                    <div class="precosColunaSegundaDet">90,00</div>
                    <div class="precosColunaSegundaDet">95,00</div>
                    <div class="precosColunaSegundaDet">183,00</div>
                </div>
                    
                <div class="precosColunaTerceira">
                    <div class="precosColunaTerceiraDetLin">Pernoite</div>
                    <div class="precosColunaTerceiraDet">120,00</div>
                    <div class="precosColunaTerceiraDet">125,00</div>
                    <div class="precosColunaTerceiraDet">135,00</div>
                    <div class="precosColunaTerceiraDet">150,00</div>
                    <div class="precosColunaTerceiraDet">160,00</div>
                    <div class="precosColunaTerceiraDet">260,00</div>
                </div>
                    
                <div class="precosColunaQuarta">
                    <div class="precosColunaQuartaDetLin">Diária</div>
                    <div class="precosColunaQuartaDet">170,00</div>
                    <div class="precosColunaQuartaDet">175,00</div>
                    <div class="precosColunaQuartaDet">180,00</div>
                    <div class="precosColunaQuartaDet">200,00</div>
                    <div class="precosColunaQuartaDet">220,00</div>
                    <div class="precosColunaQuartaDet">260,00</div>
                </div>

                <div class="precosColunaQuarta">
                    <div class="precosColunaQuartaDetLin">Horas Adicionais</div>
                    <div class="precosColunaQuartaDet">26,00</div>
                    <div class="precosColunaQuartaDet">28,00</div>
                    <div class="precosColunaQuartaDet">30,00</div>
                    <div class="precosColunaQuartaDet">32,00</div>
                    <div class="precosColunaQuartaDet">39,00</div>
                    <div class="precosColunaQuartaDet">-</div>
                </div>

                </div>

                
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>

