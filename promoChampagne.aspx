﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="promoChampagne.aspx.cs" Inherits="promoChampagne" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="includes/principal.css" rel="stylesheet" type="text/css" />
</head>
<body onLoad="self.print();">
    <form id="form1" runat="server">
    <table width="450" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="91"><img src="images/topoPopUp.jpg" width="450" height="91"></td>
  </tr>
  <tr valign="top">
    <td align="left" background="images/bgPopUp.jpg" style="padding:20px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="20" class="tahoma11-000000">Nome: <strong><asp:Literal ID="litNome" runat="server"></asp:Literal></strong> </td>
      </tr>
	     <tr>
        <td height="20" class="tahoma11-000000">Data: <strong><%= DateTime.Now.ToShortDateString() %></strong> </td>
      </tr>
     <!-- <tr>
        <td height="20" class="tahoma11-000000">V&aacute;lido at&eacute;: <strong>25/06/2006</strong> </td>
      </tr> -->
      <tr>
        <td class="tahoma11-000000">&nbsp;</td>
      </tr>
      <tr>
        <td height="30" align="center" bgcolor="#013300" class="tahoma11-FFFFFF"><strong>Vantagens na apresenta&ccedil;&atilde;o deste cupom </strong></td>
      </tr>
      <tr>
        <td class="tahoma11-000000">&nbsp;</td>
      </tr>
      <tr>
        <td class="tahoma11-000000"><span class="tahoma11-000000"><strong>Champgne (a critério do motel)</strong></span><br>
          <br>
		Nas suítes Lamour, Louvre, Lemolin e Molin Rouge<br>
<br>
<strong>Condições de utilização: </strong><br>
<br>
<p>Válido na semana do aniversário,somente nas suítesLamour, Louvre e Molin Rouge.<br>
Cupom só terá validade mediante apresentação do Rg. ,do aniversariante.</p>
<p>Imprima e apresente na entrada.se apresentado ap&oacute;s  a entrada  n&atilde;o ter&aacute; valor.<br>
  Promo&ccedil;&atilde;o n&atilde;o cumulativa.n&atilde;o poder&aacute; ser usado em conjunto c/ outras promo&ccedil;&otilde;es.<br>
  <br>
  -V&Aacute;LIDO SOMENTE PARA PGTO EM DINHEIRO.<br>
</p>
</td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td height="48"><img src="images/rodapePopUp.jpg" width="450" height="48"></td>
  </tr>
</table>
    </form>
</body>
</html>
