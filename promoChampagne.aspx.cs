﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using luverModel;

public partial class promoChampagne : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            var data = new luverEntities();
            int id = Convert.ToInt32(Request.QueryString["id"]);
            var cadastro = (from c in data.tbcadastro where c.idCadastro == id select c).FirstOrDefault();
            if (cadastro != null)
            {
                litNome.Text = cadastro.nome;
            }
            else
            {
                Response.Redirect("promocoes.aspx");
            }
        }
        else
        {
            Response.Redirect("promocoes.aspx");
        }
    }
}