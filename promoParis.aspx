﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="promoParis.aspx.cs" Inherits="promoParis" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="includes/principal.css" rel="stylesheet" type="text/css" />
</head>
<body onLoad="self.print();">
    <form id="form1" runat="server">
    <table width="450" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="91"><img src="images/topoPopUp.jpg" width="450" height="91"></td>
  </tr>
  <tr valign="top">
    <td align="left" background="images/bgPopUp.jpg" style="padding:20px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="20" class="tahoma11-000000">Nome: <strong><asp:Literal ID="litNome" runat="server"></asp:Literal></strong> </td>
      </tr>
      <tr>
        <td height="20" class="tahoma11-000000">Data: <strong><%= DateTime.Now.ToShortDateString() %></strong> </td>
      </tr>
     <!-- <tr>
        <td height="20" class="tahoma11-000000">V&aacute;lido at&eacute;: <strong>25/06/2006</strong> </td>
      </tr> -->
      <tr>
        <td class="tahoma11-000000">&nbsp;</td>
      </tr>
      <tr>
        <td height="30" align="center" bgcolor="#013300" class="tahoma11-FFFFFF"><strong>Vantagens na apresenta&ccedil;&atilde;o deste cupom </strong></td>
      </tr>
      <tr>
        <td class="tahoma11-000000">&nbsp;</td>
      </tr>
      <tr>
        <td class="tahoma11-000000">
<strong>Per&iacute;odo de  1  Hora  R$ 50,00.</strong><br>
Todos os dias.<br>
Somente na suíte Paris.<br>
<br>
<strong>Condições de utilização: </strong><br>
<p>N&Atilde;O &Eacute; V&Aacute;LIDO EM V&Eacute;SPERAS DE FERIADOS,DIA DOS NAMORADOS, E DATAS ESPECIAS.</p>
<p>IMPRIMA E APRESENTE NA ENTRADA.SE APRESENTADO AP&Oacute;S  A ENTRADA  N&Atilde;O TER&Aacute; VALOR.<br>
  PROMO&Ccedil;&Atilde;O N&Atilde;O CUMULATIVA.N&Atilde;O PODER&Aacute; SER USADO EM CONJUNTO C/ OUTRAS PROMO&Ccedil;&Otilde;ES.</p>
<p>- V&Aacute;LIDO SOMENTE PARA PGTO EM DINHEIRO.<br>
  - NECESSARIO APRESENTA&Ccedil;AO DO RG. DO EMITENTE<br>
  - CUPOM V&Aacute;LIDO POR 10 DIAS AP&Oacute;S A EMISS&Atilde;O</p></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="48"><img src="images/rodapePopUp.jpg" width="450" height="48"></td>
  </tr>
</table>
    </form>
</body>
</html>
