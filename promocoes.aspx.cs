﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using luverModel;

public partial class promocoes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnGerarCupom1_Click(object sender, ImageClickEventArgs e)
    {
        using (var data = new luverEntities())
        {
            var email = (from c in data.tbcadastro where c.email == txtEmailCupom1.Text select c).FirstOrDefault();
            if (email != null)
            {
                Response.Redirect("promoParis.aspx?id=" + email.idCadastro);
            }
            else
            {
                AlertShow("Seu e-mail não foi encontrado. Para gerar seu cupom é necessário efetuar seu cadastro!");
            }
        }
       
    }
    protected void btnGerarCupom2_Click(object sender, ImageClickEventArgs e)
    {
        using (var data = new luverEntities())
        {
            var email = (from c in data.tbcadastro where c.email == txtEmailCupom2.Text select c).FirstOrDefault();
            if (email != null)
            {
                Response.Redirect("promoSuiteParis.aspx?id=" + email.idCadastro);
            }
            else
            {
                AlertShow("Seu e-mail não foi encontrado. Para gerar seu cupom é necessário efetuar seu cadastro!");
            }
        }
       
    }
    protected void btnGerarCupom3_Click(object sender, ImageClickEventArgs e)
    {
        using (var data = new luverEntities())
        {
            var email = (from c in data.tbcadastro where c.email == txtEmailCupom3.Text select c).FirstOrDefault();
            if (email != null)
            {
                Response.Redirect("promoChampagne.aspx?id=" + email.idCadastro);
            }
            else
            {
                AlertShow("Seu e-mail não foi encontrado. Para gerar seu cupom é necessário efetuar seu cadastro!");
            }
        }
        
    }
    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "');", true);
    }
}