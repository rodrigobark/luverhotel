﻿<%@ Page Title="Suíte Lemolin | Luver Hotel" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="suiteParis.aspx.cs" Inherits="suitesDetalhes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="includes/funcoes.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="conteudointernas">
        <div class="alinhaconteudointernas">
        <div class="titleinternas"><img src="imagens/title_suites.png" /></div>
        <div class="titleinternasSegundo"><img src="imagens/title_suites2.png" /></div>
            <div class="internas">
                <div class="suitesDet">
                <div class="suitesDetImg"><img src="imagens/Lemolin.jpg" /></div>
                <div class="suitesDetTitle"><img src="imagens/LemolinTitle.jpg" /></div>
                <div class="suitesDetDesc">TV 42² (widscreen), TV a cabo ( net )
de 2 à 4 canais eróticos (para todos gostos)
home cinema c/ cd, dvd, usb, divx e rádio fm
internet(wi-fi).</div>
                <div class="suitesDetTour"><a href="#" onClick="javascript: onClick=abrePopUp('tour/Lemolin_Praia_Suite.ipx.html','Fotos','scrollbars=no,width=320, height=240'); return false;"><img src="imagens/suitesTour.jpg" /></a></div>
                <div class="suitesDetCurtir"><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.luverhotel.com.br/suiteLemonin.aspx&amp;send=false&amp;layout=standard&amp;width=450&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=80&amp;appId=184976601578822" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:80px;" allowTransparency="true"></iframe></div>
                <div class="suitesDetReservas"><img src="imagens/suitesReserva.jpg" /></div>
                </div>

                <div class="suitesDetFotos">
                    <div class="suitesDetFotosTitle"><img src="imagens/titleFotos.jpg" /></div>
                    <div class="barrasuitesDetFotos"><img src="imagens/barraFotos.jpg" /></div>

                    <div class="fotos">
                          <span class="imgfotos"><a href="fotosSuites/lemolin1.jpg" rel="lightbox[fotosSuites]"><img src="fotosSuites/lemolin1mini.jpg" /></a></span>
                          <span class="imgfotos"><a href="fotosSuites/lemolin2.jpg" rel="lightbox[fotosSuites]"><img src="fotosSuites/lemolin2mini.jpg" /></a></span>
                          <span class="imgfotos"><a href="fotosSuites/lemolin3.jpg" rel="lightbox[fotosSuites]"><img src="fotosSuites/lemolin3mini.jpg" /></a></span>
                          <span class="imgfotos"><a href="fotosSuites/lemolin4.jpg" rel="lightbox[fotosSuites]"><img src="fotosSuites/lemolin4mini.jpg" /></a></span><br /><br />
                          <span class="imgfotos"><a href="fotosSuites/lemolin5.jpg" rel="lightbox[fotosSuites]"><img src="fotosSuites/lemolin5mini.jpg" /></a></span>
                          <span class="imgfotos"><a href="fotosSuites/lemolin6.jpg" rel="lightbox[fotosSuites]"><img src="fotosSuites/lemolin6mini.jpg" /></a></span>
                          <span class="imgfotos"><a href="fotosSuites/lemolin7.jpg" rel="lightbox[fotosSuites]"><img src="fotosSuites/lemolin7mini.jpg" /></a></span>
                          <span class="imgfotos"><a href="fotosSuites/lemolin8.jpg" rel="lightbox[fotosSuites]"><img src="fotosSuites/lemolin8mini.jpg" /></a></span><br />
                     </div>

                     <div class="barrasuitesDetFotos"><img src="imagens/barraFotos.jpg" /></div>

                     <div class="suitesDetPrecos">
                        <div class="suitesDetPernoite">
                            <div class="suitesDetPernoiteTitle"><img src="imagens/titlePernoite.jpg" /></div>
                            <div class="suitesDetPernoiteDesc">
                            Domingo a Quinta - a partir das 20:00 </br> 
                            Sexta e Sábado: a partir das 5:00 da manhã</br>
                            </div>
                        </div>
                        <div class="suitesDetValor">
                            <div class="suitesDetPrecosTitle"><img src="imagens/titlePrecos.jpg" /></div>
                            <div class="suitesDetPrecosDesc">
                            Domingo a Quinta</br>
                            R$ 95,00 - período</br>
                            R$ 140,00 - pernoite</br>
                            Sexta e Sábado</br>
                            R$ 98,00 - período</br>
                            R$ 160,00 - pernoite</br>
                            Diária</br>
                            R$ 220,00</br>

                            Periodo de 3 horas R$ 90,00
                            </div>
                        </div>
                        <div class="suitesDetPeriodo">
                            <div class="suitesDetPeriodoTitle"><img src="imagens/titlePeriodo.jpg" /></div>
                            <div class="suitesDetPeriodoDesc">
                            De Domingo a Quinta - 3 horas</br> 
                            Sexta e Sábado -  a partir das 00:00 até as 6:00 da manhã 2 horas</br> 
                            </div>
                        </div>
                     </div>
                </div>
                
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Content>

