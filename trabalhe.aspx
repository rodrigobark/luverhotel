﻿<%@ Page Title="Trabalhe conosco | Luver Hotel" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="trabalhe.aspx.cs" Inherits="trabalhe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="conteudointernas">
        <div class="alinhaconteudointernas">
        <div class="titleinternas"><img src="imagens/title_trabalhe.png" /></div>
        <div class="internas">
        <div class="trabalhe">
            <div class="trabalheTextoDesc">
            Venha fazer parte da nossa equipe de colaboradores.
Preencha o formulário ao lado envie seu currículo e o nosso RH terá o seu cadastro arquivado para avaliação em nosso processo seletivo.
            </div>

            <div class="trabalheFormulario">
                <div class="trabalheFormularioInfo">
                    <img src="imagens/trabalheNome_.jpg" />
                </div><asp:TextBox ID="txtNome" runat="server" CssClass="trabalheNome"></asp:TextBox><br />

                <div class="trabalheFormularioInfo">
                    <img src="imagens/trabalheEmail_.jpg" />
                </div><asp:TextBox ID="txtEmail" runat="server" CssClass="trabalheEmail"></asp:TextBox><br />

                <div class="trabalheFormularioInfo">
                    <img src="imagens/trabalheTelefone_.jpg" />
                </div><asp:TextBox ID="txtTelefone" runat="server" CssClass="trabalheTelefone"></asp:TextBox><br />

                <div class="trabalheFormularioInfo">
                    <img src="imagens/trabalheCurriculo_.jpg" />
                </div><asp:FileUpload ID="txtCurriculo" runat="server" />
                <div style="margin-top: 5px;">
                    <asp:ImageButton ID="btnEnviar" runat="server" ImageUrl="imagens/submit.jpg" 
                        onclick="btnEnviar_Click" />
                </div>
            </div>
        </div>
        </div>
        </div>
    </div>
</asp:Content>
