﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class trabalhe : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnEnviar_Click(object sender, ImageClickEventArgs e)
    {
        MailMessage mailMessage = new MailMessage();
        mailMessage.From = new MailAddress("luver@luverhotel.com.br", txtNome.Text);
        mailMessage.To.Add(new MailAddress("luver@luverhotel.com.br"));
        //mailMessage.To.Add(new MailAddress("andre@bark.com.br"));
        try
        {
            mailMessage.ReplyTo = new MailAddress(txtEmail.Text);
        }
        catch (Exception)
        {

        }

        if (txtCurriculo.PostedFile != null && txtCurriculo.FileName != "")
        {
            string fileName = Path.GetFileName(txtCurriculo.PostedFile.FileName);
            Attachment myAttachment = new Attachment(txtCurriculo.FileContent, fileName);
            mailMessage.Attachments.Add(myAttachment);
        }
        StringBuilder body = new StringBuilder();
        body.AppendFormat("<strong>Dados Pessoais:</strong><br>");
        body.AppendFormat("Nome: {0}<br>", txtNome.Text);
        body.AppendFormat("E-mail: {0}<br>", txtEmail.Text);
        body.AppendFormat("Telefone: {0}<br>", txtTelefone.Text);
        mailMessage.Subject = "Envio de Currículo - Luver Hotel";
        mailMessage.Body = body.ToString();
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;
        SmtpClient smtpClient = new SmtpClient("127.0.0.1");
        try
        {
            smtpClient.Send(mailMessage);
            AlertShow("Currículo enviado com sucesso");
        }
        catch
        {

        }
    }
    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", string.Concat("alert(\'", message, "\');"), true);
    }
}