﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="usrRodape.ascx.cs" Inherits="usrRodape" %>
<div class="rodape">
        <div class="alinharodape">
            <div class="logorodape"><a href="default.aspx"><img src="imagens/logorodape.png" /></a></div>
            <div class="endrodape">Rua: Frei Caneca, nº. 963 - Cerqueira César - São Paulo - SP</div>
            <div class="emailrodape"><a href="mailto:luver@luverhotel.com.br">luver@luverhotel.com.br</a></div>
            <div class="whats">
                <img src="imagens/whats.png" alt="WhatsApp"> <p> (11) 97370-6687</p>
            </div>
            <div class="telrodape"> (11) 3262-3375</div>
            <div class="redessociais">
                <a href="eCard.aspx" style="text-align:right; color:#FFFFFF; text-decoration:none;">Recomende a um amigo</a><br />
                <a href="eCard.aspx"><img src="imagens/recomende.jpg" /></a>
                <a href="https://www.facebook.com/LuverHotelSaoPaulo" target="_blank"><img src="imagens/facebook.png" /></a>
                <a href="http://twitter.com/#!/luverhotel" target="_blank"><img src="imagens/twitter.png" /></a>
            </div>
            <div class="bark">
                <a href="http://www.bark.com.br" target="_blank" title="Criação de Sites"><img src="imagens/bark.png" alt="Desenvolvimento de Sites" /></a>
            </div>
        </div>
</div>